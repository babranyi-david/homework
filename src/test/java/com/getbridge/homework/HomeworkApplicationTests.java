package com.getbridge.homework;

import com.getbridge.homework.model.Discussion;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.of;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD)
class HomeworkApplicationTests {

    private static final String BASE_PATH = "/discussions";
    private static final String DISCUSSION_ID = "discussion_id";
    private static final String TITLE = "title";
    private static final long TIMESTAMP = 0;
    private static final String DESCRIPTION = "description";
    private static final String LOCATION = "location";
    private static final String USER_ID_1 = "user_id1";
    private static final String USER_ID_2 = "user_id2";
    private static final List<String> USER_IDS = List.of(USER_ID_1, USER_ID_2);
    private static final boolean IS_CLOSED = false;
    private static final Discussion USER_1_DISCUSSION = new Discussion(DISCUSSION_ID, TITLE, TIMESTAMP, DESCRIPTION, LOCATION, USER_IDS, IS_CLOSED);
    private static final String AUTHENTICATION_HEADER = "X-AUTHENTICATED-USER";


    @Autowired
    private TestRestTemplate testRestTemplate;

    @ParameterizedTest
    @MethodSource("provideInvalidUserIdLists")
    void shouldReturnNElementWhenNElementCreated(List<Discussion> discussions, int expectedSize) {
        // given
        discussions.forEach(
                discussion -> testRestTemplate.postForEntity(BASE_PATH, createHttpEntity(discussion, USER_ID_1), Discussion.class));

        // when
        var response = testRestTemplate.exchange(BASE_PATH, GET, createHttpEntity(USER_ID_1), Discussion[].class);

        // then
        assertThat(response.getBody()).hasSize(expectedSize);
    }

    @Test
    void shouldReturnEmptyListWhenASingleElementDeleted() {
        // given
        var postResponse = testRestTemplate.postForEntity(BASE_PATH, createHttpEntity(USER_1_DISCUSSION, USER_ID_1), Discussion.class);
        var id = postResponse.getBody().getId();
        testRestTemplate.exchange(BASE_PATH + "/" + id, DELETE, createHttpEntity(USER_ID_1), Void.class);

        // when
        var response = testRestTemplate.exchange(BASE_PATH, GET, createHttpEntity(USER_ID_1), Discussion[].class);

        // then
        assertThat(response.getBody()).hasSize(0);
    }

    @Test
    void shouldReturnModifiedElementAfterModification() {
        // given
        var postResponse = testRestTemplate.postForEntity(BASE_PATH, createHttpEntity(USER_1_DISCUSSION, USER_ID_1), Discussion.class);
        var id = postResponse.getBody().getId();
        var new_title = "new title";
        var modifiedDiscussion = new Discussion(id, new_title, TIMESTAMP, DESCRIPTION, LOCATION, USER_IDS, IS_CLOSED);

        // when
        testRestTemplate.exchange(BASE_PATH + "/" + id, PUT, createHttpEntity(modifiedDiscussion, USER_ID_1), Discussion.class);

        // then
        Discussion[] responseDiscussions = testRestTemplate.exchange(BASE_PATH, GET, createHttpEntity(USER_ID_1), Discussion[].class).getBody();
        assertThat(responseDiscussions).hasSize(1);
        assertThat(responseDiscussions[0].getTitle()).isEqualTo(new_title);
    }

    /* TODO: Add integration test scenarios
        - when a discussion is closed then it cannot be modified via PUT
        - when a discussion is closed then it can be deleted via DELETE
        - when a GET all discussion called with ?isClosed=(true, false) filter then only the relevant discussions returned
        - Auth
            - user cannot create discussions for others with POST
            - user can only see discussions belongs to him/her
            - user cannot update discussion which not belongs to him/her
            - user cannot delete discussion which not belongs to him/her
     */

    public static Stream<Arguments> provideInvalidUserIdLists() {
        return Stream.of(
                of(List.of(), 0),
                of(List.of(USER_1_DISCUSSION), 1),
                of(List.of(USER_1_DISCUSSION, USER_1_DISCUSSION), 2)
        );
    }

    private HttpEntity<Discussion> createHttpEntity(Discussion discussion, String userId) {
        return new HttpEntity<>(discussion, createAuthHeader(userId));
    }

    private HttpEntity<Object> createHttpEntity(String userId) {
        return new HttpEntity<>(createAuthHeader(userId));
    }

    private HttpHeaders createAuthHeader(String userId) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(AUTHENTICATION_HEADER, userId);
        return headers;
    }


}
