package com.getbridge.homework.util;

import com.getbridge.homework.model.Discussion;

import java.util.List;
import java.util.UUID;

public class DiscussionUtils {
    public static Discussion createDiscussionWithUsers(String userId1, String userId2) {
        var discussion = new Discussion();
        discussion.setUserIds(List.of(userId1, userId2));
        return discussion;
    }

    public static Discussion createDiscussionWithRandomId(String userId1, String userId2) {
        var discussion = createDiscussionWithUsers(userId1, userId2);
        discussion.setId(UUID.randomUUID().toString());
        return discussion;
    }
}
