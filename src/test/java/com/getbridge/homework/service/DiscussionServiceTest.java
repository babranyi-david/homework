package com.getbridge.homework.service;

import com.getbridge.homework.exception.DiscussionNotAuthorizedException;
import com.getbridge.homework.model.Discussion;
import com.getbridge.homework.repository.DiscussionRepository;
import com.getbridge.homework.util.DiscussionUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.getbridge.homework.util.DiscussionUtils.createDiscussionWithRandomId;
import static com.getbridge.homework.util.DiscussionUtils.createDiscussionWithUsers;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DiscussionServiceTest {
    private static final String USER_ID_1 = "user_id1";
    private static final String USER_ID_2 = "user_id2";
    private static final String USER_ID_3 = "user_id3";
    private static final boolean IS_CLOSED = false;

    private DiscussionService discussionService;

    @Mock
    private DiscussionRepository discussionRepository;

    @BeforeEach
    void setUp() {
        discussionService = new DiscussionService(discussionRepository);
    }

    @Test
    void ShouldReturnElementsFromRepositoryWhenAllDiscussionRequestedWithUserId() {
        // given
        when(discussionRepository.findByUserIds(USER_ID_1)).thenReturn(List.of(new Discussion()));

        // when
        List<Discussion> result = discussionService.getDiscussions(USER_ID_1);

        // then
        assertThat(result).isEqualTo(List.of(new Discussion()));
    }

    @Test
    void ShouldReturnElementsFromRepositoryWhenAllDiscussionRequestedWithUserIdAndIsClosed() {
        // given
        when(discussionRepository.findByIsClosedAndUserIds(IS_CLOSED, USER_ID_1)).thenReturn(List.of(new Discussion()));

        // when
        List<Discussion> result = discussionService.getDiscussions(USER_ID_1, IS_CLOSED);

        // then
        assertThat(result).isEqualTo(List.of(new Discussion()));
    }

    @Test
    void shouldThrowExceptionWhenUserTriesToCreateDiscussionWithoutItself() {
        var discussion = createDiscussionWithUsers(USER_ID_1, USER_ID_2);

        assertThrows(DiscussionNotAuthorizedException.class,
                () -> discussionService.createDiscussion(USER_ID_3, discussion));
    }

    @Test
    void shouldReturnSavedElementWhenNewDiscussionCreated() {
        // given
        var discussionInput = createDiscussionWithUsers(USER_ID_1, USER_ID_2);
        var repositoryResponse = createDiscussionWithRandomId(USER_ID_1, USER_ID_2);
        when(discussionRepository.save(discussionInput)).thenReturn(repositoryResponse);


        // when
        Discussion result = discussionService.createDiscussion(USER_ID_1, discussionInput);

        // then
        assertThat(result).isEqualTo(repositoryResponse);
    }

    @Test
    void shouldDeleteDiscussionWhenUserIsAuthorizedToDeleteTheDiscussion() {
        // given
        var discussion = createDiscussionWithRandomId(USER_ID_1, USER_ID_2);
        when(discussionRepository.findById(discussion.getId())).thenReturn(Optional.of(discussion));

        // when
        discussionService.deleteDiscussion(USER_ID_1, discussion.getId());

        // then
        verify(discussionRepository, times(1)).deleteById(discussion.getId());
    }

    /*TODO: Test scenarios
        - Delete:
            - throw exception when not authorized
        - Update:
            - throw exception when not found
            - throw exception when closed
            - throw exception when not authorized
            - replace id if provided
            - return saved discussion
        - allow user to create discussion with only itself
     */


}