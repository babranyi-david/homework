package com.getbridge.homework.repository;

import com.getbridge.homework.model.Discussion;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;
import java.util.stream.Stream;

import static com.getbridge.homework.util.DiscussionUtils.createDiscussionWithUsers;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.of;

@DataJpaTest
class DiscussionRepositoryTest {
    private static final String USER_ID_1 = "user_id1";
    private static final String USER_ID_2 = "user_id2";
    private static final String USER_ID_3 = "user_id3";

    @Autowired
    private DiscussionRepository discussionRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @ParameterizedTest
    @MethodSource("provideSavedDiscussions")
    void shouldReturnDiscussionsBelongsToTheUserWhenDiscussionsRequested(List<Discussion> discussions, int expectedResultCount) {
        // given
        discussions.forEach(discussion -> testEntityManager.persist(discussion));
        testEntityManager.flush();
        testEntityManager.clear();

        // when
        List<Discussion> result = discussionRepository.findByUserIds(USER_ID_1);

        // then
        assertThat(result).hasSize(expectedResultCount);
    }

    /* TODO: add test scenarios
        - findByIsClosedAndUserIds returns only filtered discussions (isClosed and userId match)
        - createDiscussion fills id with UUID
        - deleteById only deletes discussion with the specified id

     */

    public static Stream<Arguments> provideSavedDiscussions() {
        return Stream.of(
                of(List.of(), 0),
                of(List.of(createDiscussionWithUsers(USER_ID_2, USER_ID_3)), 0),
                of(List.of(createDiscussionWithUsers(USER_ID_1, USER_ID_2)), 1),
                of(List.of(createDiscussionWithUsers(USER_ID_1, USER_ID_2), createDiscussionWithUsers(USER_ID_2, USER_ID_3)), 1),
                of(List.of(createDiscussionWithUsers(USER_ID_1, USER_ID_2), createDiscussionWithUsers(USER_ID_3, USER_ID_1)), 2)
        );
    }
}