package com.getbridge.homework.controller;

import com.getbridge.homework.exception.DiscussionNotAuthorizedException;
import com.getbridge.homework.model.Discussion;
import com.getbridge.homework.service.DiscussionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.util.List;
import java.util.stream.Stream;

import static com.getbridge.homework.util.JsonTestUtils.asJsonString;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.params.provider.Arguments.of;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class DiscussionControllerTest {

    private static final String DISCUSSION_ID = "discussion_id";
    private static final String TITLE = "title";
    private static final long TIMESTAMP = 0;
    private static final String DESCRIPTION = "description";
    private static final String LOCATION = "location";
    private static final String USER_ID_1 = "user_id1";
    private static final String USER_ID_2 = "user_id2";
    private static final List<String> USER_IDS = List.of(USER_ID_1, USER_ID_2);
    private static final boolean IS_CLOSED = false;
    private static final Discussion DISCUSSION = new Discussion(DISCUSSION_ID, TITLE, TIMESTAMP, DESCRIPTION, LOCATION, USER_IDS, IS_CLOSED);
    private static final String BASE_URL = "/discussions";
    private static final String AUTHENTICATION_HEADER = "X-AUTHENTICATED-USER";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DiscussionService discussionServiceMock;


    //TODO: Test response status and body for all endpoints
    @Test
    void shouldReturn201WithServiceResultWhenDiscussionCreated() throws Exception {
        // given
        given(discussionServiceMock.createDiscussion(USER_ID_1, DISCUSSION)).willReturn(DISCUSSION);

        // when
        mockMvc.perform(createPostJsonBuilder()
                        .header(AUTHENTICATION_HEADER, USER_ID_1))

                // then
                .andExpect(status().isCreated())
                .andExpect(content().json(asJsonString(DISCUSSION)));
    }

    //TODO: Header check for all endpoints
    @Test
    void shouldReturn400WhenAuthenticationHeaderMissing() throws Exception {
        // given
        given(discussionServiceMock.createDiscussion(USER_ID_1, DISCUSSION)).willReturn(DISCUSSION);

        // when
        mockMvc.perform(createPostJsonBuilder())

                // then
                .andExpect(status().isBadRequest());
    }

    //TODO: Test for all service exceptions and endpoints
    @Test
    void shouldReturn403WhenUserWantsToCreateDiscussionForOthers() throws Exception {
        // given
        given(discussionServiceMock.createDiscussion(any(), any())).willThrow(new DiscussionNotAuthorizedException());

        // when
        mockMvc.perform(createPostJsonBuilder()
                        .header(AUTHENTICATION_HEADER, "a_third_user_id"))

                // then
                .andExpect(status().isForbidden());
    }

    //TODO: Test for all input violations for all endpoints
    @ParameterizedTest
    @MethodSource("provideInvalidUserIdLists")
    void shouldReturn400WhenTheUserIdCountForADiscussionIsNotTwo(List<String> userIds) throws Exception {
        // given
        Discussion inputDiscussion = new Discussion(DISCUSSION_ID, TITLE, TIMESTAMP, DESCRIPTION, LOCATION, userIds, IS_CLOSED);

        // when
        mockMvc.perform(createPostJsonBuilder(inputDiscussion)
                        .header(AUTHENTICATION_HEADER, USER_ID_1))

                // then
                .andExpect(status().isCreated());
    }

    public static Stream<Arguments> provideInvalidUserIdLists() {
        return Stream.of(
                of(List.of()),
                of(List.of(USER_ID_1)),
                of(List.of(USER_ID_1, USER_ID_2, USER_ID_1))
        );
    }

    private MockHttpServletRequestBuilder createPostJsonBuilder() {
        return createPostJsonBuilder(DISCUSSION);
    }

    private MockHttpServletRequestBuilder createPostJsonBuilder(Discussion discussion) {
        return post(BASE_URL)
                .contentType(APPLICATION_JSON)
                .content(asJsonString(discussion))
                .characterEncoding(UTF_8.name());
    }
}