package com.getbridge.homework.controller;

import com.getbridge.homework.model.Discussion;
import com.getbridge.homework.service.DiscussionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.getbridge.homework.constant.Constants.USER_AUTHENTICATION_HEADER;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping(path = "/discussions")
@RequiredArgsConstructor
public class DiscussionController {
    private final DiscussionService discussionService;

    @GetMapping
    public List<Discussion> getAll(@RequestParam Optional<Boolean> isClosed,
                                   @RequestHeader(USER_AUTHENTICATION_HEADER) String userId) {
        return isClosed.map(closed -> discussionService.getDiscussions(userId, closed))
                .orElse(discussionService.getDiscussions(userId));
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Discussion createDiscussion(@RequestBody Discussion discussion,
                                       @RequestHeader(USER_AUTHENTICATION_HEADER) String userId) {
        return discussionService.createDiscussion(userId, discussion);
    }

    @PutMapping("{id}")
    public Discussion updateDiscussion(@PathVariable UUID id,
                                       @RequestBody Discussion discussion,
                                       @RequestHeader(USER_AUTHENTICATION_HEADER) String userId) {
        return discussionService.updateDiscussion(userId, id.toString(), discussion);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteDiscussion(@PathVariable UUID id, @RequestHeader(USER_AUTHENTICATION_HEADER) String userId) {
        discussionService.deleteDiscussion(userId, id.toString());
    }

}
