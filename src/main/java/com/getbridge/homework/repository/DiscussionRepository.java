package com.getbridge.homework.repository;

import com.getbridge.homework.model.Discussion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DiscussionRepository extends JpaRepository<Discussion, String> {
    public List<Discussion> findByIsClosedAndUserIds(boolean isClosed, String userId);
    public List<Discussion> findByUserIds(String userId);
}
