package com.getbridge.homework.constant;

public record Constants() {
    public static final String USER_AUTHENTICATION_HEADER = "X-AUTHENTICATED-USER";
}
