package com.getbridge.homework.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(indexes = {@Index(name = "is_closed_index", columnList = "is_closed")})
public class Discussion {
    @Id
    @GeneratedValue(generator = "uuid2", strategy = IDENTITY)
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    private String title;

    private Long startTime;

    private String description;

    private String location;

    @ElementCollection
    @CollectionTable(name = "participants",
            joinColumns = @JoinColumn(name = "id"),
            indexes = {@Index(name = "user_id_index", columnList = "user_id")})
    @Column(name = "user_id")
    @Size(min = 2, max = 2)
    private List<String> userIds;

    @Column(name = "is_closed")
    private boolean isClosed = false;
}
