package com.getbridge.homework.service;

import com.getbridge.homework.exception.DiscussionIsClosedException;
import com.getbridge.homework.exception.DiscussionNotAuthorizedException;
import com.getbridge.homework.exception.DiscussionNotFoundException;
import com.getbridge.homework.model.Discussion;
import com.getbridge.homework.repository.DiscussionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DiscussionService {
    private final DiscussionRepository discussionRepository;

    public List<Discussion> getDiscussions(String userId) {
        return discussionRepository.findByUserIds(userId);
    }

    public List<Discussion> getDiscussions(String userId, boolean isClosed) {
        return discussionRepository.findByIsClosedAndUserIds(isClosed, userId);
    }

    public Discussion createDiscussion(String userId, Discussion discussion) {
        checkIfAuthorized(userId, discussion);
        return discussionRepository.save(discussion);
    }

    public void deleteDiscussion(String userId, String id) {
        Optional<Discussion> discussion = discussionRepository.findById(id);
        if (discussion.isPresent()) {
            checkIfAuthorized(userId, discussion.get());
            discussionRepository.deleteById(id);
        }
    }

    public Discussion updateDiscussion(String userId, String id, Discussion discussionInput) {
        Discussion discussion = discussionRepository.findById(id).orElseThrow(DiscussionNotFoundException::new);
        checkIfClosed(discussion);
        checkIfAuthorized(userId, discussion);
        discussionInput.setId(id);
        return discussionRepository.save(discussionInput);
    }

    private void checkIfClosed(Discussion discussion) {
        if (discussion.isClosed()) {
            throw new DiscussionIsClosedException();
        }
    }

    private void checkIfAuthorized(String userId, Discussion discussion) {
        if (!isAuthorized(userId, discussion)) {
            throw new DiscussionNotAuthorizedException();
        }
    }

    private boolean isAuthorized(String userId, Discussion discussion) {
        return discussion.getUserIds().contains(userId);
    }
}
