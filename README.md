# homework

## Start application
- Build application
```shell
./gradlew build
```
- Running the containers
```shell
docker-compose up --build
```

## Use application

Call `localhost:8080/discussions` or use the provided postman collection `Homework.postman_collection.json`
