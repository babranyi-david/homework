FROM amazoncorretto:17-alpine-jdk
COPY build/libs/homework-0.0.1-SNAPSHOT.jar homework.jar
ENTRYPOINT ["java","-jar","/homework.jar"]